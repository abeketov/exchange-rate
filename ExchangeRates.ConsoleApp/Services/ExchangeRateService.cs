﻿using ExchangeRates.ConsoleApp.Constants;
using ExchangeRates.ConsoleApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRates.ConsoleApp.Services
{
    public interface IExchangeRateService
    {
        Task ExecuteAsync();
    }

    internal class ExchangeRateService : IExchangeRateService
    {
        public ExchangeRateService()
        {

        }

        public async Task ExecuteAsync()
        {
            using var client = new HttpClient();

            var pairs = new List<CurrencyPair>
            {
                new CurrencyPair
                {
                    FromCurrency = CurrencyCode.CURRENCY_CODE_USD,
                    ToCurrency = CurrencyCode.CURRENCY_CODE_JPY
                },
                new CurrencyPair
                {
                    FromCurrency = CurrencyCode.CURRENCY_CODE_BTC,
                    ToCurrency = CurrencyCode.CURRENCY_CODE_CNY
                },

            };

            var sb = new StringBuilder();

            //var apiKey = "WTSIJ7QTW3X289YF";
            var apiKey = "demo";

            while (true)
            {
                sb.Clear();
                foreach(var pair in pairs)
                {
                    var queryUrl = $"https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency={pair.FromCurrency}&to_currency={pair.ToCurrency}&apikey={apiKey}";

                    var response = await client.GetAsync(queryUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Response>(content);

                        if (data?.RealtimeCurrencyExchangeRate == null)
                        {
                            sb.AppendLine(content);
                        }
                        else
                        {
                            sb.AppendLine(data.RealtimeCurrencyExchangeRate.ToString());
                        }
                    }
                }

                Console.Clear();
                Console.WriteLine(sb);
                await Task.Delay(1000);
            }
        }
    }
}
