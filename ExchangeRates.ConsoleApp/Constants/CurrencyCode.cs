﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRates.ConsoleApp.Constants
{
    public static class CurrencyCode
    {
        public const string CURRENCY_CODE_USD = "USD";

        public const string CURRENCY_CODE_EUR = "EUR";

        public const string CURRENCY_CODE_CNY = "CNY";

        public const string CURRENCY_CODE_BTC = "BTC";

        public const string CURRENCY_CODE_JPY = "JPY";
    }
}
