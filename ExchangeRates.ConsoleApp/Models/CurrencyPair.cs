﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRates.ConsoleApp.Models
{
    public class CurrencyPair
    {
        public string FromCurrency { get; set; }

        public string ToCurrency { get; set; }
    }
}
