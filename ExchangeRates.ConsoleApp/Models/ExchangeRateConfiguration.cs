﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRates.ConsoleApp.Models
{
    public class ExchangeRateConfiguration
    {
        public string ApiKey { get; set; }

        public string EndpointUrl { get; set; }

        public List<CurrencyPair> Pairs { get; set; } = new();

        public int RefreshInterval { get; set; }
    }
}
