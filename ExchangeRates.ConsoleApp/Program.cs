﻿using ExchangeRates.ConsoleApp.Models;
using ExchangeRates.ConsoleApp.Services;

internal class Program
{
    static async Task Main(string[] args)
    {
        IExchangeRateService service = new ExchangeRateService();

        await service.ExecuteAsync();
    }
}